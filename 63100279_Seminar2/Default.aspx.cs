﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _63100279_Seminar2
{
    public partial class Default : System.Web.UI.Page
    {
        ServiceReference1.HRMServiceClient s;
        protected void Page_Load(object sender, EventArgs e)
        {
            s = new ServiceReference1.HRMServiceClient();
            if (!IsPostBack)
            {
                var q = (from p in s.GetDepartmentList()
                         group p by new { p.DepartmentGroupName }
                             into mygroup
                         select mygroup.First()).OrderBy(x => x.DepartmentGroupName);

                DDL_DepartmentGroup.DataSource = q;
                DDL_DepartmentGroup.DataTextField = "DepartmentGroupName";
                DDL_DepartmentGroup.DataValueField = "DepartmentGroupName";
                DDL_DepartmentGroup.DataBind();

                DDL_DepartmentGroup_SelectedIndexChanged(sender, e);
                DDL_Department_SelectedIndexChanged(this.DDL_Department, e);
            }
        }

        protected void DDL_DepartmentGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            var q = (from p in s.GetDepartmentList()
                     where p.DepartmentGroupName.Equals(DDL_DepartmentGroup.SelectedValue.ToString())
                     group p by new { p.DepartmentName } 
                            into mygroup
                    select mygroup.First()).OrderBy(x => x.DepartmentName);                    

           DDL_Department.DataSource = q;
            DDL_Department.DataTextField = "DepartmentName";
            DDL_Department.DataValueField = "DepartmentID";
            DDL_Department.DataBind();

            DDL_Department_SelectedIndexChanged(sender, e);

        }

        protected void setEmployeeGrid(int newPageIndex)
        {
            var q = (from p in s.GetEmployeeListForDepartment(Convert.ToInt32(DDL_Department.SelectedValue))
                     select new
                     {
                         ImeInPriimek = p.FirstName + " " + p.LastName,
                         DelovnoMesto = p.JobTitle,
                         DatumZaposlitve = p.HireDate,
                         EmployeeID = p.EmployeeID
                     });

            GV_Employee.PageIndex = newPageIndex;
            GV_Employee.VirtualItemCount = q.Count();
            GV_Employee.DataSource = q.Skip(GV_Employee.PageSize * newPageIndex).Take(GV_Employee.PageSize).ToList();
            GV_Employee.DataBind();

            B_Orders.Visible = false;
            L_SaleStatistics1.Visible = false;
            L_SaleStatistics2.Visible = false;
            L_SaleStatistics3.Visible = false;
            GV_Orders.Visible = false;
        }

        protected void DDL_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            setEmployeeGrid(0);
            GV_Employee.SelectedIndex = -1;
            DV_EmployeeDetails.Visible = false;
        }

        protected void GV_Employee_SelectedIndexChanged(object sender, EventArgs e)
        {
            setEmployeeGrid(GV_Employee.PageIndex);            
            DV_EmployeeDetails.Visible = true;

            var q = (from p in s.GetEmployeeList()
                     where p.EmployeeID.Equals(Convert.ToInt32(GV_Employee.SelectedValue))                     
                     let years = DateTime.Now.Year - p.HireDate.Year
                     let birthdayThisYear = p.HireDate.AddYears(years)
                     select new { ImeInPriimek = p.FirstName + " " + p.LastName,  
                                  Gender = p.Gender,
                                  BirthDate = p.BirthDate,                                  
                                  Address = p.Address,
                                  City = p.City + " " + p.PostalCode,
                                  State = p.State,
                                  Country = p.Country,
                                  PhoneNumber = p.PhoneNumber,
                                  EmailAddress = p.EmailAddress,
                                  HireDate = p.HireDate,
                                  DelovnaDoba = (birthdayThisYear > DateTime.Now ? years - 1 : years) + " let", 
                                  JobTitle = p.JobTitle,
                                  Izmena = p.Shift + " ( " + p.ShiftStartTime + " - " + p.ShiftEndTime + " )",
                                  PayRate = p.PayRate});

            DV_EmployeeDetails.DataSource = q.ToList();
            DV_EmployeeDetails.DataBind();

            if (DDL_Department.SelectedItem.Text.Equals("Sales") && GV_Employee.SelectedIndex != -1)
            {
                B_Orders.Visible = true;
            }
        }

        protected void setOrdersGrid(int newPageIndex)
        {
            var q = (from p in s.GetOrderList(Convert.ToInt32(GV_Employee.SelectedValue))
                      select new
                      {
                          OrderNumber = p.OrderNumber,
                          OrderDate = p.OrderDate,
                          Customer = p.Customer,
                          Amount = p.Amount,
                          SubTotal = p.SubTotal,
                          TaxAmt = p.TaxAmt,
                          Freight = p.Freight,
                          TotalDue = p.TotalDue
                      });

            GV_Orders.PageIndex = newPageIndex;
            GV_Orders.VirtualItemCount = q.Count();
            GV_Orders.DataSource = q.Skip(GV_Orders.PageSize * newPageIndex).Take(GV_Orders.PageSize).ToList();
            GV_Orders.DataBind();
        }

        protected void B_Orders_Click(object sender, EventArgs e)
        {
            setEmployeeGrid(GV_Employee.PageIndex);

            var q = (from p in s.GetOrderList(Convert.ToInt32(GV_Employee.SelectedValue))
                select p);

            L_SaleStatistics2.Text = "Število naročil: " + q.Count().ToString();

            var q3 = (from p in s.GetOrderList(Convert.ToInt32(GV_Employee.SelectedValue))
                select p.SubTotal).Sum();


            L_SaleStatistics3.Text = "Skupna vrednost naročil: " + String.Format("{0:C}", q3);

            var q2 = (from p in q
                      group p by new { p.Customer }
                          into customers
                      select customers);

            L_SaleStatistics1.Text = "Število strank: " + q2.Count().ToString();
            setOrdersGrid(0);

            L_SaleStatistics1.Visible = true;
            L_SaleStatistics2.Visible = true;
            L_SaleStatistics3.Visible = true;
            GV_Orders.Visible = true;
            B_Orders.Visible = true;
        }

        protected void GV_Employee_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            setEmployeeGrid(e.NewPageIndex);
        }

        protected void GV_Orders_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            setOrdersGrid(e.NewPageIndex);
        }
    }
}