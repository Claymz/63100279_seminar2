﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_63100279_Seminar2.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">

    
        <h1>HRM</h1>


    
        <hr />
        <b>Organizacijska enota:</b>&nbsp;
    
        <asp:DropDownList ID="DDL_DepartmentGroup" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DDL_DepartmentGroup_SelectedIndexChanged" style="height: 22px">
        </asp:DropDownList>
    
        <br/>
        <b>Oddelek:</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:DropDownList ID="DDL_Department" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DDL_Department_SelectedIndexChanged">
        </asp:DropDownList>
        <hr />
        <br />
        <asp:GridView ID="GV_Employee" runat="server" AllowCustomPaging="True" AllowPaging="True" AutoGenerateColumns="False" DataKeyNames="EmployeeID" OnSelectedIndexChanged="GV_Employee_SelectedIndexChanged" PageSize="5" OnPageIndexChanging="GV_Employee_PageIndexChanging" Font-Bold="True" GridLines="Horizontal" Width="100%">
            <Columns>
                <asp:BoundField DataField="ImeInPriimek" HeaderText="Ime in priimek" />
                <asp:BoundField DataField="DelovnoMesto" HeaderText="Delovno mesto" />
                <asp:BoundField DataField="DatumZaposlitve" HeaderText="Datum zaposlitve" DataFormatString="{0:d}" />
                <asp:CommandField SelectText="Podrobnosti" ShowSelectButton="True" >
                <ItemStyle HorizontalAlign="Right" />
                </asp:CommandField>
            </Columns>
            <HeaderStyle BackColor="Black" ForeColor="White" />
            <PagerSettings Mode="NextPrevious" />
            <PagerStyle BackColor="Black" ForeColor="White" />
            <SelectedRowStyle BackColor="Silver" />
        </asp:GridView>

        <br />

        <asp:DetailsView ID="DV_EmployeeDetails" runat="server" AutoGenerateRows="False" Height="50px" Visible="False" Width="100%" FooterText="/" GridLines="None" HeaderText="/">
            <Fields>
                <asp:BoundField DataField="ImeInPriimek" HeaderText="Ime in priimek:" >
                <HeaderStyle BackColor="Silver" Font-Bold="True" Width="30%" />
                </asp:BoundField>
                <asp:BoundField DataField="Gender" HeaderText="Spol:" >
                <HeaderStyle BackColor="Silver" Font-Bold="True" Width="30%" />
                </asp:BoundField>
                <asp:BoundField DataField="BirthDate" DataFormatString="{0:d}" HeaderText="Datum rojstva:" >
                <HeaderStyle BackColor="Silver" Font-Bold="True" Width="30%" />
                </asp:BoundField>
                <asp:BoundField>
                <HeaderStyle BackColor="Silver" Width="30%" />
                </asp:BoundField>
                <asp:TemplateField HeaderText="Naslov:">

                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Bind("Address") %>'></asp:Label>
                        <br />
                        <asp:Label ID="Label6" runat="server" Text='<%# Bind("City") %>'></asp:Label>
                        <br />
                        <asp:Label ID="Label7" runat="server" Text='<%# Bind("State") %>'></asp:Label>
                        <br />
                        <asp:Label ID="Label8" runat="server" Text='<%# Bind("Country") %>'></asp:Label>
                    </ItemTemplate>

                    <HeaderStyle BackColor="Silver" Font-Bold="True" VerticalAlign="Top" Width="30%" />

                </asp:TemplateField>
                <asp:BoundField>
                <HeaderStyle BackColor="Silver" Width="30%" />
                </asp:BoundField>
                <asp:BoundField DataField="PhoneNumber" HeaderText="Telefonska številka:" >
                <HeaderStyle BackColor="Silver" Font-Bold="True" Width="30%" />
                </asp:BoundField>
                <asp:BoundField DataField="EmailAddress" HeaderText="Elektronski naslov:" >
                <HeaderStyle BackColor="Silver" Font-Bold="True" Width="30%" />
                </asp:BoundField>
                <asp:BoundField>
                <HeaderStyle BackColor="Silver" Width="30%" />
                </asp:BoundField>
                <asp:BoundField DataField="HireDate" DataFormatString="{0:d}" HeaderText="Datum zaposlitve:" >
                <HeaderStyle BackColor="Silver" Font-Bold="True" Width="30%" />
                </asp:BoundField>
                <asp:BoundField DataField="DelovnaDoba" HeaderText="Delovna doba:" >
                <HeaderStyle BackColor="Silver" Font-Bold="True" Width="30%" />
                </asp:BoundField>
                <asp:BoundField DataField="JobTitle" HeaderText="Delovno mesto:" >
                <HeaderStyle BackColor="Silver" Font-Bold="True" Width="30%" />
                </asp:BoundField>
                <asp:BoundField DataField="Izmena" HeaderText="Izmena:" >
                <HeaderStyle BackColor="Silver" Font-Bold="True" Width="30%" />
                </asp:BoundField>
                <asp:BoundField>
                <HeaderStyle BackColor="Silver" Width="30%" />
                </asp:BoundField>
                <asp:BoundField DataField="PayRate" DataFormatString="{0:c}" HeaderText="Urna postavka:" >
                <HeaderStyle BackColor="Silver" Font-Bold="True" Width="30%" />
                </asp:BoundField>
            </Fields>
            <FooterStyle BackColor="Black" />
            <HeaderStyle BackColor="Black" />
        </asp:DetailsView>
        <hr />
        <br />
        <asp:Button ID="B_Orders" runat="server" OnClick="B_Orders_Click" Text="Prikaži ustvarjen promet" Visible="False" />
        <br />
        <br />
        <asp:Label ID="L_SaleStatistics1" runat="server" Visible="False"></asp:Label>
        <br />
        <asp:Label ID="L_SaleStatistics2" runat="server" Visible="False"></asp:Label>
        <br />
        <asp:Label ID="L_SaleStatistics3" runat="server" Visible="False"></asp:Label>
        <br />
        <br />
        <asp:GridView ID="GV_Orders" runat="server" AllowCustomPaging="True" AllowPaging="True" AutoGenerateColumns="False" OnPageIndexChanging="GV_Orders_PageIndexChanging" Visible="False" GridLines="None" PageSize="20" Width="100%">
            <Columns>
                <asp:BoundField DataField="OrderNumber" HeaderText="Naročilo" />
                <asp:BoundField DataField="OrderDate" DataFormatString="{0:d}" HeaderText="Datum" />
                <asp:BoundField DataField="Customer" HeaderText="Stranka" />
                <asp:BoundField DataField="Amount" HeaderText="Število izdelkov" >
                <HeaderStyle HorizontalAlign="Right" />
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField DataField="Subtotal" DataFormatString="{0:c}" HeaderText="Vrednost" >
                <HeaderStyle HorizontalAlign="Right" />
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField DataField="TaxAmt" DataFormatString="{0:c}" HeaderText="Davek" >
                <HeaderStyle HorizontalAlign="Right" />
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField DataField="Freight" DataFormatString="{0:c}" HeaderText="Dostava" >
                <HeaderStyle HorizontalAlign="Right" />
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
                <asp:BoundField DataField="TotalDue" DataFormatString="{0:c}" HeaderText="Za plačilo">
                <HeaderStyle HorizontalAlign="Right" />
                <ItemStyle HorizontalAlign="Right" />
                </asp:BoundField>
            </Columns>
            <HeaderStyle BackColor="Black" ForeColor="White" />
            <PagerSettings Mode="NextPrevious" />
            <PagerStyle BackColor="Black" ForeColor="White" />
        </asp:GridView>
    </form>
</body>
</html>
